package main

import (
	"database/sql"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	//	"os/user"
	"path/filepath"
	"strconv"
	"time"

	"./icon"

	"github.com/getlantern/systray"
	_ "github.com/mattn/go-sqlite3"
	"github.com/sparrc/go-ping"
	"github.com/sqweek/dialog"
)

type Config struct {
	Site    string  `json:"site"`
	Goodval float64 `json:"good_val"`
	Count   int     `json:"count"`
	Wait    float32 `json:"wait"`
}

func checkErr(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
		panic(-1)
	}
}

func getLog(csvFile string) {

	dbPath := filepath.Join(_current_dir(), "db", "db.sqlite")
	db, _ := sql.Open("sqlite3", dbPath)
	rows, err := db.Query(`select transmitted, received, lost, min, max,
							  average, stddev, time from packets`)
	checkErr("Error in select query:", err)
	var (
		transmitted int
		received    int
		lost        int
		min         float64
		max         float64
		average     float64
		stddev      float64
		time        string
	)

	file, err := os.Create(csvFile)
	checkErr("Cannot create file", err)
	defer file.Close()
	writer := csv.NewWriter(file)
	defer writer.Flush()
	writer.Write([]string{"transmitted", "received", "lost", "min", "max",
		"average", "stddev", "time"})
	for rows.Next() {
		err = rows.Scan(&transmitted, &received, &lost, &min, &max, &average, &stddev, &time)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(transmitted, received, lost, min, max, average, stddev, time)

		err = writer.Write([]string{strconv.Itoa(transmitted),
			strconv.Itoa(received), strconv.Itoa(lost),
			strconv.FormatFloat(min, 'f', -1, 64),
			strconv.FormatFloat(max, 'f', -1, 64),
			strconv.FormatFloat(average, 'f', -1, 64),
			strconv.FormatFloat(stddev, 'f', -1, 64),
			time})
		checkErr("Cannot write to file", err)
	}
	systray.SetTooltip("Export Completed")

}

func insertData(transmitted int, received int, lost float64, min float64,
	max float64, avg float64, stddev float64, startTime string) {
	dbPath := filepath.Join(_current_dir(), "db", "db.sqlite")
	db, _ := sql.Open("sqlite3", dbPath)
	defer db.Close()
	statement, _ := db.Prepare(`INSERT INTO packets (transmitted, received,
                                lost, min, max, average, stddev, time) 
                                VALUES (?, ?, ?, ?, ?, ?, ?, ?)`)
	statement.Exec(transmitted, received, lost, min,
		max, avg, stddev, startTime)
}

func exportDB() {
	filename, err := dialog.File().Filter("CSV files", "csv").Title("Export Log file").Save()
	checkErr("CSV file not selected", err)
	/* myself, error := user.Current()
	if error != nil {
		panic(error)
	}
	getLog(filepath.Join(myself.HomeDir, "export.csv"))
	*/
	getLog(filename)
}

func onReady() {
	systray.SetIcon(icon.GreenIcon)
	conf := getConfig()
	mCheck := systray.AddMenuItem("Check BW", "Check the Bandwidth")
	mExport := systray.AddMenuItem("Export", "Export Bandwidth log as csv file")
	mQuit := systray.AddMenuItem("Quit", "Quit the whole app")
	go func() {
		for {
			pingHost(conf)
			wait := time.Duration(conf.Wait) * time.Minute
			time.Sleep(wait)
		}
	}()

	go func() {
		for {
			select {
			case <-mCheck.ClickedCh:
				pingHost(conf)
			case <-mExport.ClickedCh:
				exportDB()
				systray.SetTooltip("Export Completed...")
			case <-mQuit.ClickedCh:
				systray.SetTooltip("Exiting ....")
				systray.Quit()
				onExit()
				return
			}
		}
	}()
}

func onExit() {

}

func _current_dir() string {
	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	return filepath.Dir(ex)

}

func getConfig() Config {
	confFilePath := filepath.Join(_current_dir(), "config.json")
	fmt.Println(confFilePath)
	file, _ := ioutil.ReadFile(confFilePath)
	data := Config{}
	_ = json.Unmarshal([]byte(file), &data)
	return data
}

func pingHost(conf Config) {
	systray.SetTooltip("Pinging server")

	goodval := conf.Goodval
	systray.SetIcon(icon.BlueIcon)

	pinger, _ := ping.NewPinger(conf.Site)
	pinger.Count = conf.Count
	now := time.Now().Format("2006-01-02 15:04:05")
	tooltip := fmt.Sprintf("Pinging: %v", conf.Site)
	systray.SetTooltip(tooltip)
	pinger.Run() // blocks until finished
	stats := pinger.Statistics()
	average := float64(stats.AvgRtt) / float64(time.Millisecond)
	min := float64(stats.MinRtt) / float64(time.Millisecond)
	max := float64(stats.MaxRtt) / float64(time.Millisecond)
	stddev := float64(stats.StdDevRtt) / float64(time.Millisecond)
	if average > goodval {
		systray.SetIcon(icon.RedIcon)
	} else {
		systray.SetIcon(icon.GreenIcon)
	}
	tooltip = fmt.Sprintf("Time Recorded: %v\nAverage: %v\nMinimum: %v\nMaximum: %v\nStdDev:  %v,\nPacket Lost: %v",
		now, average, min, max, stddev, stats.PacketLoss)
	systray.SetTooltip(tooltip)
	insertData(stats.PacketsSent, stats.PacketsRecv, stats.PacketLoss, min, max, average, stddev, now)
}

func main() {
	systray.Run(onReady, onExit)
}
