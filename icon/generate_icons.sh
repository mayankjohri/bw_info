~/go/bin/2goarray BlueIcon icon < speedometer_blue.png > blue_icon.go
~/go/bin/2goarray RedIcon icon < speedometer_red.png > red_icon.go
~/go/bin/2goarray GreenIcon icon < speedometer_green.png > green_icon.go

mkdir icons.iconset/
sizes=(16 32 64 128 256 512)
for SIZE in "${sizes[@]}"
do
  echo $SIZE
  sips -z $SIZE $SIZE  bw_info_1024.png --out icons.iconset/icon_${SIZE}x${SIZE}.png
done
cd icons.iconset
cp 'icon_32x32.png'     'icon_16x16@2x.png'
mv 'icon_64x64.png'     'icon_32x32@2x.png'
cp 'icon_256x256.png'   'icon_128x128@2x.png'
cp 'icon_512x512.png'   'icon_256x256@2x.png'
cd ..
iconutil -c icns -o ../pkgs/mac/bw_info.app/Resources/icon.icns icons.iconset

